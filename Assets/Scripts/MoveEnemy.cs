
using UnityEngine;
using System.Collections;

public class MoveEnemy : MonoBehaviour
{
	// 状態の名称を定義する
	public enum State
    {
		Wait,
		Search,
		Attack
    }

	// 初期の状態を指定する
	public State state = State.Wait;

	private CharacterController enemyController;
	private Animator animator;
	//　目的地
	public Vector3 destination;
	//　歩くスピード
	[SerializeField]
	private float walkSpeed = 1.0f;
	//　速度
	private Vector3 velocity;
	//　移動方向
	private Vector3 direction;
	//　到着フラグ
	private bool arrived;
	//　スタート位置
	private Vector3 startPosition;

	GameObject PlayerTarget;
	Transform target;
	public GameObject EBS;
	Weapon weapon;

	// Use this for initialization
	void Start()
	{
		PlayerTarget = GameObject.Find("PlayerCenter");
		target = PlayerTarget.transform;
		weapon = EBS.GetComponent<Weapon>();
		enemyController = GetComponent<CharacterController>();
		animator = GetComponent<Animator>();
		startPosition = transform.position;
		velocity = Vector3.zero;
		arrived = false;
		weapon.enabled = false;
	}

	void UpdateWait()
    {
		Invoke("UpdateSearch", 3.2f);
    }

	void UpdateSearch()
    {
		if (!arrived)
		{
			if (enemyController.isGrounded)
			{
				velocity = Vector3.zero;
				animator.SetFloat("Speed", 2.0f);
				direction = (destination - transform.position).normalized;
				transform.LookAt(new Vector3(destination.x, transform.position.y, destination.z));
				velocity = direction * walkSpeed;
			}
			velocity.y += Physics.gravity.y * Time.deltaTime;
			enemyController.Move(velocity * Time.deltaTime);

			//　目的地に到着したかどうかの判定
			if (Vector3.Distance(transform.position, destination) <0.5f)
			{
				arrived = true;
				animator.SetFloat("Speed", 0.0f);
			}
		}
		else
        {
			Invoke("UpdateAttack", 1.0f);
        }
	}

	void UpdateAttack()
    {
		weapon.enabled = true;
		transform.LookAt(target);

	}

	// Update is called once per frame
	void Update()
	{
		// 状態によって動作を分岐
		switch(state)
        {
			case State.Wait:
				UpdateWait();
				break;
			case State.Search:
				UpdateSearch();
				break;
			case State.Attack:
				UpdateAttack();
				break;
        }

		
	}
}