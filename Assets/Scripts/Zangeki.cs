using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zangeki : MonoBehaviour
{
    void Start()
    {
        Renderer mr = GetComponent<Renderer>();
        Color color = mr.material.color;
        color.a = 1f;
        color.r = 100;
        mr.material.color = color;
    }
}
