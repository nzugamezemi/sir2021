using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SowrdSound : MonoBehaviour
{

    public AudioSource audioSource_1;
    public AudioSource audioSource_2;
    public AudioSource audioSource_3;
    public AudioClip swing;
    public AudioClip collision;
    public AudioClip swingCollision;
    TrailRenderer trailRenderer;
    Transform afterEffect;

    bool tof = false;

    public void Swing()
    {
        audioSource_1.clip=swing;
        audioSource_1.PlayOneShot(swing);
    }

    public void Collision()
    {
        audioSource_2.clip = collision;
        audioSource_2.PlayOneShot(collision);
    }

    public void SwingCollision()
    {
        audioSource_3.clip = swingCollision;
        audioSource_3.PlayOneShot(swingCollision);
    }

    void Start()
    {
        afterEffect = transform.GetChild(5);
        trailRenderer = afterEffect.GetComponent<TrailRenderer>();

    }

    void Update()
    {
        if(trailRenderer.emitting == true)
        {
            if(tof == false)
            {
                Swing();
                tof = true;
            }
        }
        else
        {
            tof = false;
        }
    }
}
