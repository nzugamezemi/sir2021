using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int life = 0;
    public int lifeMax = 0;
    public bool heal = false;
    public AudioClip DamageSound;
    public AudioClip healSound;
    public Slider HP;
    public GameObject healEffect;
    Transform healPosition;
    GameObject player;
    AudioSource audioSource;

    float time;
    Scene scene;

    void Start()
    {
        healPosition = transform.GetChild(0);
        audioSource = GetComponent<AudioSource>();
        HP.value = life;
        player = transform.root.gameObject;
        scene = player.GetComponent<Scene>();
    }

    private void Update()
    {
        HP.value = life;
        time += Time.deltaTime;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            life -= 1;

            audioSource.clip = DamageSound;
            audioSource.PlayOneShot(DamageSound);

            
            if (life <= 0)
            {
                if (scene.name == Scene.SceneName.TutorialScene)
                {
                    life = 3;
                }
                else
                {
                    if (scene.name == Scene.SceneName.MainScene)
                    {
                        SceneManager.LoadScene(2);
                    }

                    if (scene.name == Scene.SceneName.MainHardScene)
                    {
                        SceneManager.LoadScene(7);
                    }

                }
            }
        }
    }

    
    public void OnHeal(InputAction.CallbackContext context)
    {
        if (life > 0 && life < lifeMax)
        {
            if(time >= 5)
            {
                if (context.phase == InputActionPhase.Started)
                {
                    life += 1;
                    time = 0;
                    Instantiate(healEffect, healPosition.position, healPosition.rotation);
                    audioSource.clip = healSound;
                    audioSource.PlayOneShot(healSound);
                    heal = true;
                }
            }
        }
    }
}
