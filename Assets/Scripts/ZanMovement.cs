using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZanMovement : MonoBehaviour
{
    float time = 0;
    float alpha = 1f;

    Material material_ue;
    Material material_sita;

    Transform ue;
    Transform sita;

    void Start()
    {
        ue = transform.GetChild(0);
        sita = transform.GetChild(1);

        material_ue = ue.GetComponent<Renderer>().material;
        material_sita = sita.GetComponent<Renderer>().material;
    }

    void Update()
    {
        time += Time.deltaTime;
        if(time >= 1.0)
        {
            material_ue.SetColor("_Alpha", new Color(0, 0, 0, alpha));
            material_sita.SetColor("_Alpha", new Color(0, 0, 0, alpha));

            alpha -= 0.01f;
        }

         if (time >= 1.6)
        {
            Destroy(gameObject);

        }
    }
}
