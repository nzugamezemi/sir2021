using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour
{
    public int phase = 0;
    public float count = 0;
    bool tf = false;
    bool swingTF = true;
    public GameObject player;
    public GameObject playerBody;
    public GameObject sowrd;
    public GameObject enemy;
    public GameObject enemyGun;
    public GameObject afterEffect;

    Afterimage afterimage;
    Sword sword;
    Scene scene;
    Player playerHP;

    void Start()
    {
        enemy.SetActive(false);
        enemyGun.SetActive(false);
        afterimage = afterEffect.GetComponent<Afterimage>();
        sword = sowrd.GetComponent<Sword>();
        scene = player.GetComponent<Scene>();
        playerHP = playerBody.GetComponent<Player>();
    }

    void SwingPhase()
    {
        if (swingTF && afterimage.tf)
        {
            count += 1;
            swingTF = false;
            
            if(count == 50)
            {
                phase += 1;
                count = 0;
            }
        }
        else
        {
            swingTF = true;
        }
    }

    void SlashPhase()
    {
        count = sword.count;
        enemyGun.SetActive(true);
        playerHP.life = 3;
        if(count >= 3)
        {
            phase += 1;
        }
    }

    void AttackPhase()
    {
        count = scene.enemycount - 1;
        if(enemyGun != null)
        {
            enemyGun.SetActive(false);
        }
        
        sword.count = 3;
        if(count == 0)
        {
            playerHP.life = 2;
            phase += 1;
        }
    }

    void HealPhase()
    {
        count = playerHP.life;
        if(count >= 3)
        {
            phase += 1;
        }
    }

    void VSPhase()
    {
        count = scene.enemycount;
        if (!tf)
        {
            enemy.SetActive(true);
            tf = true;
        }
    }

    void Update()
    {
        switch (phase)
        {
            case 0:
                SwingPhase();
                break;
            case 1:
                SlashPhase();
                break;
            case 2:
                AttackPhase();
                break;
            case 3:
                HealPhase();
                break;
            case 4:
                VSPhase();
                break;
        }
    }
}
