using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    public GameObject xrrig; 
    public GameObject trigerObject;
    public GameObject StartObject;
    public GameObject RetryObject;
    GameObject EasyObject;
    GameObject HardObject;
    Scene scene;

    void Start()
    {
        if(xrrig != null)
        {
            scene = xrrig.GetComponent<Scene>();
        }

        EasyObject = scene.easyObject;
        HardObject = scene.hardObject;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject == trigerObject)
        {
            scene.enter = true;
        }

        if (other.gameObject == StartObject)
        {
            scene.start = true;
        }

        if (other.gameObject == RetryObject)
        {
            scene.retry = true;
        }

        if (other.gameObject == EasyObject)
        {
            scene.easy = true;
        }

        if (other.gameObject == HardObject)
        {
            scene.hard = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == trigerObject)
        {
            scene.enter = false;
        }
        if (other.gameObject == StartObject)
        {
            scene.start = false;
        }

        if (other.gameObject == RetryObject)
        {
            scene.retry = false;
        }

        if (other.gameObject == EasyObject)
        {
            scene.easy = false;
        }

        if (other.gameObject == HardObject)
        {
            scene.hard = false;
        }
    }
}
