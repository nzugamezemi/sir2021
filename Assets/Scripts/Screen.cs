using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Screen : MonoBehaviour
{
    public GameObject instructions;
    public GameObject count;
    public GameObject exolanation;
    public GameObject sceneController;
    public GameObject healBottonImage;
    public GameObject attackBottonImage;

    float time;
    int second;

    Text instructText;
    Text countText;
    Text explanationText;
    Tutorial tutorial;
    
    void Start()
    {
        countText = count.GetComponent<Text>();
        instructText = instructions.GetComponent<Text>();
        explanationText = exolanation.GetComponent<Text>();
        tutorial = sceneController.GetComponent<Tutorial>();

        explanationText.text = "";
        time = 5;
        healBottonImage.SetActive(false);
        attackBottonImage.SetActive(false);
    }

    private void Update()
    {
        
        switch (tutorial.phase)
        {
            case 0:
                Swing();
                break;
            case 1:
                Slash();
                break;
            case 2:
                Attack();
                break;
            case 3:
                Heal();
                break;
            case 4:
                VS();
                break;
        }
    }

    void Swing()
    {
        instructText.text = "刀を振ろう！";
        countText.text = tutorial.count + " / 50";
    }

    void Slash()
    {
        instructText.text = "弾を斬ろう！";
        countText.text = tutorial.count + " / 3";
    }

    void Attack()
    {
        instructText.text = "斬撃を飛ばそう！";
        countText.text = tutorial.count + " / 3";
        explanationText.text = "振りながら黄色ボタンを押そう";
        attackBottonImage.SetActive(true);
    }

    void Heal()
    {
        instructText.text = "回復しよう！";
        countText.text = tutorial.count + " / 3";
        explanationText.text = "緑の色ボタンを押そう";
        attackBottonImage.SetActive(false);
        healBottonImage.SetActive(true);
    }

    void VS()
    {
        instructText.text = "敵を倒そう！";
        countText.text = tutorial.count + " / 1";
        explanationText.text = "";
        healBottonImage.SetActive(false);

        if (tutorial.count == 0)
        {
            time -= Time.deltaTime;
            second = (int)time;
            instructText.text = "施設に乗り込むぞ！";
            countText.text = second.ToString();
        }
    }
}
