using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(VRController))]
public class VRThrow : MonoBehaviour
{
    public Transform anchor = null;
    public GameObject ballPrefab = null;
    public int throwCount = 0;
    public GameObject swordRotate = null;
    GameObject ball;

    VRController vrController;
    Sword sword;
    Afterimage afterimage;
    Transform afterEffect;

    private void Start()
    {
        vrController = GetComponent<VRController>();
        sword = GetComponent<Sword>();
        afterEffect = transform.GetChild(5);
        afterimage = afterEffect.GetComponent<Afterimage>();
    }

    

    public void Throw(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            if (ballPrefab != null && anchor != null)
            {
                if (sword.count >= sword.attack)
                {
                    if(sword.tof == true)
                    {
                        if(vrController.velocity.x >= afterimage.velocity || vrController.velocity.y >= afterimage.velocity || vrController.velocity.z >= afterimage.velocity || vrController.velocity.x <= afterimage.velocity * -1 || vrController.velocity.y <= afterimage.velocity * -1 || vrController.velocity.z <= afterimage.velocity * -1)
                        {
     
                            ball = Instantiate(ballPrefab, anchor.position, swordRotate.transform.rotation);
                            

                            throwCount++;

                            if (throwCount == 1)
                            {
                                throwCount = 0;
                                sword.count = 0;
                            }
                        }
                    }
                }
            }
        }
    }
}
