using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Quaternion firstRotation;
    public float speedZ = 0;
    public float speedXrange = 0;
    public float speedYrange = 0;
    public float lifetimeSeconds = 0;

    private void Start()
    {
        float speedX = Random.Range(-1 * speedXrange, speedXrange);
        float speedY = Random.Range(-1 * speedYrange, speedYrange);
        
        firstRotation = transform.rotation;
        Rigidbody rigidbody = GetComponent<Rigidbody>();

        Vector3 movementSpeed = new Vector3(speedX, speedY, speedZ);

        movementSpeed = firstRotation * movementSpeed;

        rigidbody.AddForce(movementSpeed);

        StartCoroutine(DestroyAfterWait());
    }

    IEnumerator DestroyAfterWait()
    {
        yield return new WaitForSeconds(Mathf.Max(0, lifetimeSeconds));

        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "PlayerBody")
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Objects")
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Sword")
        {
            Destroy(gameObject);
        }
    }
}
