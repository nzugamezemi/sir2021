using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MagnetBall : MonoBehaviour
{

    public Vector3 direction = Vector3.forward;
    public float speed = 1;
    public float power = 1f;
    public float area = 1f;
    public float curve = 2f;

    List<Transform> enemyTransforms = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
        enemyTransforms = GameObject.FindGameObjectsWithTag("Enemy").Select(o => o.transform).ToList();
    }

    public float a;

    // Update is called once per frame
    void Update()
    {
        direction.Normalize();

        foreach(Transform t in enemyTransforms)
        {
            Vector3 delta = t.position - transform.position;
            float d = delta.magnitude;
            Vector3 dir = delta.normalized;

            a = (1f - Mathf.Clamp(Mathf.Pow(d / area, curve), 0f, 1f)) * power;
            

            direction = Vector3.Lerp(direction, dir, a);
        }

        transform.position += direction * speed;
    }
}
