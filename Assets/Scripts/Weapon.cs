using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject Bullet;
    GameObject player;
    public AudioClip bulletSound;
    AudioSource audioSource;
    public GameObject enemyEye;
    Material material;
    float changeTime;
    int random;
    public int firstRange;
    public int secondRange;
    bool tf = false;
    bool tf1 = false;
    float time = 0;

    Scene scene;

    private void Start()
    {
        player = GameObject.Find("Player Main");
        audioSource = GetComponent<AudioSource>();
        material = enemyEye.GetComponent<Renderer>().material;
        scene = player.GetComponent<Scene>();
    }
    void Update()
    {
        time += Time.deltaTime;

        if (!tf)
        {
            if(scene.enemycount == 1)
            {
                random = Random.Range(Mathf.Clamp(firstRange - 2, 0, 5), Mathf.Clamp(secondRange - 2, 0, 5));
                
            }
            else
            {
                random = Random.Range(firstRange, secondRange);
            }
            tf = true;
        }

        if (tf)
        {
            if (time >= random)
            {
                if (!tf1)
                {
                    changeTime = time;
                    tf1 = true;
                }
                else
                {
                    material.color = new Color(1f, 0f, 0f, 0f);
                    material.SetColor("_Alpha", new Color(1f, 0f, 0f, 0.8f));
                    if (time >= changeTime + 2f)
                    {
                        Weapon_movement();
                        time = 0;
                        tf = false;
                        tf1 = false;
                    }
                }
            }
            else
            {
                material.color = new Color(0f, 1f, 1f, 0.5f);
                material.SetColor("_Alpha", new Color(0f, 1f, 1f, 0.5f));
            }
        }
        

    }

    void Weapon_movement()
    {
        Vector3 placePosition = this.transform.position;

        Quaternion q = this.transform.rotation;

        Instantiate(Bullet, placePosition, q);

        audioSource.clip = bulletSound;
        audioSource.PlayOneShot(bulletSound);
    }
}
