using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VRThrow))]
public class Sword : MonoBehaviour
{
    public float count = 0;
    public int attack = 3;

    public bool tof = false;

    SowrdSound sowrdSound;
    Afterimage afterimage;
    Transform afterEffect;

    public GameObject bulletEffect;

    void Start()
    {
        sowrdSound = GetComponent<SowrdSound>();
        afterEffect = transform.GetChild(5);
        afterimage = afterEffect.GetComponent<Afterimage>();
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {

            Instantiate(bulletEffect, this.transform.position, this.transform.rotation);
            if (afterimage.tf)
            {
                sowrdSound.SwingCollision();
                count += 1;
            }
            else
            {
                sowrdSound.Collision();
            }
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if(other.gameObject.name == "Anchor")
        {
            tof = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Anchor")
        {
            tof = false;
        }
    }
}
