using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    Player player;
    public GameObject life;
    public int num = 0;
    GameObject child;

    void Start()
    {
        if(GameObject.FindGameObjectWithTag("Player") != null)
        {
            GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
            player = playerObject.GetComponent<Player>();
        }

        child = transform.GetChild(0).gameObject;
    }

     void Update()
    {
        if (player.life == num)
        {
            if(player.heal == false)
            {
                child.SetActive(false);
            }
        }
        if (player.life == num + 1 && player.heal == true)
        {
            child.SetActive(true);
            player.heal = false;
        }
    }
}
