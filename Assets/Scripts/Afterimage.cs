using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Afterimage : MonoBehaviour
{
    GameObject sowrd;
    VRController vrController;
    Sword sword;
    TrailRenderer trailRenderer;
    

    public bool tf = false;
    public float velocity = 0f;
    public float colorcount;
    float time;
    
    void Start()
    {
        sowrd = transform.parent.gameObject;
        vrController = sowrd.GetComponent<VRController>();
        trailRenderer = GetComponent<TrailRenderer>();
        sword = sowrd.GetComponent<Sword>();
    }

    void Update()
    {
        if (vrController.velocity.x >= velocity || vrController.velocity.y >= velocity || vrController.velocity.z >= velocity || vrController.velocity.x <= velocity * -1 || vrController.velocity.y <= velocity * -1 || vrController.velocity.z <= velocity * -1)
        {
            trailRenderer.emitting = true;
            tf = true;
        }
        else
        {
            trailRenderer.emitting = false;
            tf = false;
        }
        colorcount = sword.count;

        switch (colorcount)
        {
            case 0:
                trailRenderer.startColor = new Color(1f, 1f, 1f, 0.5f);
                break;
            case 1:
                trailRenderer.startColor = new Color(1f, 1f, 0f, 0.5f);
                break;
            case 2:
                trailRenderer.startColor = new Color(1f, 0.5f, 0f, 0.7f);
                break;
            case 3:
                trailRenderer.startColor = new Color(1f, 0f, 0f, 1f);
                break;
        }

        
    }
}
