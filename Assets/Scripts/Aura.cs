using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aura : MonoBehaviour
{
    GameObject sowrd;
    Sword sword;
    Material material;
    public AudioClip sowrdMax;
    AudioSource audioSource;

    
    void Start()
    {
        sowrd = transform.parent.gameObject;
        sword = sowrd.GetComponent<Sword>();
        material = GetComponent<Renderer>().material;
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        switch (sword.count)
        {
            case 0:
                material.color = new Color(0f, 0f, 0f, 0f);
                material.SetColor("_Alpha", new Color(0, 0, 0, 0f));
                break;
            case 1:
                material.color = new Color(1f, 1f, 0f, 0.5f);
                material.SetColor("_Alpha", new Color(0, 0, 0, 1));
                break;
            case 2:
                material.color = new Color(1f, 0.5f, 0f, 0.7f);
                material.SetColor("_Alpha", new Color(0, 0, 0, 1));
                break;
            case 3:
                material.color = new Color(1f, 0f, 0f, 1f);
                material.SetColor("_Alpha", new Color(0, 0, 0, 1));
                audioSource.clip = sowrdMax;
                audioSource.PlayOneShot(sowrdMax);
                break;
        }
    }
}
