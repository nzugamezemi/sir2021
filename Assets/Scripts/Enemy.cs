using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int life;

    public AudioClip enemyDestroy;
    public GameObject Zan;
    GameObject Player;
    Transform Zan_Position;
    Scene scene;

    void Start()
    {
        Zan_Position = transform.GetChild(1);
        Player = GameObject.FindGameObjectWithTag("Player");
        scene = Player.GetComponent<Scene>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "zangeki")
        {
            life -= 1;

            if (life == 0)
            {
                scene.enemycount -= 1;
                AudioSource.PlayClipAtPoint(enemyDestroy, transform.position);
                Instantiate(Zan, Zan_Position.position, gameObject.transform.rotation);
                Destroy(gameObject);
            }
        }
    }
}

