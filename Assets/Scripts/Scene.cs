using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class Scene : MonoBehaviour
{
    public bool enter = false;
    public bool retry = false;
    public bool start = false;
    public bool easy = false;
    public bool hard = false;
    float time;
    float resetTime;
    bool click = false;
    bool left = false;
    bool right = false;
    bool retryClick = false;
    bool startClick = false;
    bool easyClick = false;
    bool hardClick = false;
    public int enemycount;

    public GameObject easyObject;
    public GameObject hardObject;
    public GameObject screenObject;

    private void Start()
    {
        if(easyObject != null)
        {
            easyObject.SetActive(false);
        }

        if(hardObject != null)
        {
            hardObject.SetActive(false);
        }
    }

    public enum SceneName
    {
        StartScene,
        MainScene,
        MainHardScene,
        GameOverScene,
        GameOverHardScene,
        CompleteScene,
        TutorialScene,
        TutorialHardScene
    }

    public new SceneName name = SceneName.StartScene;

    private void OnClick(InputValue input)
    {
        if(enter == true)
        {
            click = true;
        }

        if (retry == true)
        {
            retryClick = true;
        }

        if (start == true)
        {
            startClick = true;
        }

        if (easy == true)
        {
            easyClick = true;
        }

        if (hard == true)
        {
            hardClick = true;
        }
    }

    public void OnLeftStick(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
        {
            left = true;
        }
        
        if(context.phase == InputActionPhase.Canceled)
        {
            left = false;
        }
    }

    public void OnRightStick(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            right = true;
        }
        
        if (context.phase == InputActionPhase.Canceled)
        {
            right = false;
        }
    }

    void StartReset()
    {
        resetTime += Time.deltaTime;
        if (resetTime >= 1.0)
        {
            SceneManager.LoadScene(0);
        }
    }

    void StartScene() {
        if (click)
        {
            easyObject.SetActive(true);
            hardObject.SetActive(true);
            screenObject.SetActive(false);
        }

        if (easyClick)
        {
            SceneManager.LoadScene(4);
        }

        if (hardClick)
        {
            SceneManager.LoadScene(6);
        }
    }

    void MainScene()
    {
        if (enemycount == 0)
        {
            time += Time.deltaTime;
            if(time >= 3)
            {
                SceneManager.LoadScene(3);
            }
        }
    }

    void GameOverScene()
    {
        if (retryClick)
        {
            if (name == SceneName.GameOverScene)
            {
                SceneManager.LoadScene(1);
            }
            if (name == SceneName.GameOverHardScene)
            {
                SceneManager.LoadScene(5);
            }
            
        }
        
        if(startClick)
        {
            SceneManager.LoadScene(0);
        }
    }

    void CompleteScene()
    {
        if (click)
        {
            SceneManager.LoadScene(0);
        }
    }

    void TutorialScene()
    {
        if (enemycount == 0)
        {
            time += Time.deltaTime;
            if(name == SceneName.TutorialScene && time >= 5)
            {
                SceneManager.LoadScene(1);
            }
            if(name == SceneName.TutorialHardScene && time >= 5)
            {
                SceneManager.LoadScene(5);
            }
        }
    }


    private void Update()
    {
        
        if (left && right)
        {
            StartReset();
        }

        switch (name)
        {
            case SceneName.StartScene:
                StartScene();
                break;
            case SceneName.MainScene:
                MainScene();
                break;
            case SceneName.MainHardScene:
                MainScene();
                break;
            case SceneName.GameOverScene:
                GameOverScene();
                break;
            case SceneName.GameOverHardScene:
                GameOverScene();
                break;
            case SceneName.CompleteScene:
                CompleteScene();
                break;
            case SceneName.TutorialScene:
                TutorialScene();
                break;
            case SceneName.TutorialHardScene:
                TutorialScene();
                break;
        }
    }
}