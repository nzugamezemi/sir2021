using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AimAssist : MonoBehaviour
{
    Vector3 direction;
    public float speed = 1;

    [Range(0, 1)]
    public float power = 1f;

    [Range(0,1)]
    public float range = 0.1f;

    AudioSource audioSource;
    public AudioClip zangekiSound;

    List<Transform> enemyTransforms = new List<Transform>();

    void Start()
    {
        enemyTransforms = GameObject.FindGameObjectsWithTag("Enemy").Select(o => o.transform).ToList();
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = zangekiSound;
        audioSource.PlayOneShot(zangekiSound);
    }

    void Update()
    {
        direction = transform.forward;
        direction.Normalize();
        
        if(enemyTransforms != null)
        {
            foreach (Transform t in enemyTransforms)
            {
                Vector3 delta = t.position - transform.position;
                Vector3 dir = delta.normalized;

                float dot = Vector3.Dot(direction, dir);
                float a = 0f;
                if (range > 0f)
                {
                    a = Mathf.Clamp(1f + (dot - 1f) / (range * 0.01f), 0f, 1f);
                }

                Debug.DrawRay(transform.position, dir * a * 10f, Color.green);

                direction = Vector3.Lerp(direction, dir, a * power);
            }
        }

        Debug.DrawRay(transform.position, direction * 5f, Color.blue);
        Debug.DrawRay(transform.position, direction * Time.deltaTime * speed, Color.red, 2f);

        transform.Translate(direction * Time.deltaTime * speed, Space.World);
    }
}
